global.$ = global.jQuery = require("jquery");
require("jquery-lazy");
require("bootstrap");

Headroom = require("headroom.js");
require("blockui-npm");
require("jquery.appear");
require("parsleyjs");
require("parsleyjs/dist/i18n/es");
require("jquery-toast-plugin");
require("owl.carousel");

$.removeLazyLoading = function($selector, $delay) {
  $($selector).lazy({
    delay: $delay,
    afterLoad: function(element) {
      element.removeClass("lazy");
    }
  });
};

$.setNavbarConfig = function(innerWidth) {
  var lastUrlSegment = $("#DATA").data("last_segment");

  if (innerWidth < 992 || lastUrlSegment != "") {
    $("#main-nav").removeClass("fixed-top");
    $("#main-nav nav")
      .removeClass("navbar-home navbar-light")
      .addClass("navbar-content navbar-dark");
    if (lastUrlSegment == "") {
      $("#main-nav nav")
        .addClass("navbar-home navbar-light")
        .removeClass("navbar-content navbar-dark");
    }
    //$("#body-content").addClass('content');
  } else {
    $("#main-nav").addClass("fixed-top");
    $("#main-nav nav").addClass("navbar-home navbar-light");
    $("#body-content").removeClass("content");
  }
};

$(function() {
  //WINDOW RESIZE REMOVE/ADD FIXED TOP NAVBAR CLASS
  $.setNavbarConfig(window.innerWidth);
  $(window).on("resize", function(ev) {
    $.setNavbarConfig(ev.target.innerWidth);
  });

  //LOADER
  $("#load").fadeOut(1000);

  //Lazy loading carousel images
  if ($(".carousel").length) {
    $(".carousel.lazy").on("slide.bs.carousel", function(ev) {
      var lazy;
      lazy = $(ev.relatedTarget).find("img[data-src]");
      lazy.attr("src", lazy.data("src"));
      lazy.removeAttr("data-src");
    });
  }

  //HEADROOM
  //HEADROOM
  var header = document.getElementById("main-nav");
  var headroom = new Headroom(header, {
    offset: 205,
    tolerance: 5,
    classes: {
      initial: "animated",
      pinned: "fadeInDown",
      unpinned: "fadeOutUp"
    }
  });
  headroom.init();

  //CAll
  $(".btn-call,[data-call]").on("click", function() {
    gtag("event", "info_call", {
      event_category: "called"
    });
    console.log("LLAMADA");
    window.open("tel:+521442011062");
    return false;
  });

  //Email
  $("[data-email]").on("click", function() {
    gtag("event", "email", {
      event_category: "email_sent"
    });
    window.open("mailto:info@kurgan.mx");
    return false;
  });

  //Whatsapp
  $(".btn-whats").on("click", function() {
    gtag("event", "message_sent", {
      event_category: "sent"
    });

    text = "Deseo comprar una casa";

    window.open(
      "https://api.whatsapp.com/send?phone=524422011062" + "&text=" + text
    );

    return false;
  });

  $("[data-whatsapp]").on("click", function() {
    window.open(
      "https://api.whatsapp.com/send?phone=524422011062" +
        "&text=" +
        $(this).data("whatsapp")
    );
  });
});
