$(function() {
  $(".owl-carousel").owlCarousel({
    nav: false,
    dots: true,
    center: true,
    autoplay: true,
    items: 1,
    loop: true,
    dotClass: "custom-dot"
  });

  $(".closeSlideModal").on("click", function() {
    $("#slideModal")
      .removeClass("slideInUp")
      .addClass("slideOutDown fast animated");
  });

  $(".modal-item").on("click", function() {
    $("#slideModal .modal-title").text($(this).data("title"));
    $("#slideModal .modal-body p").text($(this).data("text"));

    //Removes animations and hides the modal
    $("#slideModal")
      .removeClass("slideInUp slideOutDown")
      .addClass("d-none");

    //SlideInUp modal
    setTimeout(function() {
      $("#slideModal")
        .removeClass("d-none slideInUp")
        .addClass("slideInUp fast animated");
    }, 200);
  });

  //Animations on slide

 /*  $("#description").appear(function() {
    $(this)
      .find("p")
      .addClass("fadeInDown animated slow");
  });

  $("#about-us").appear(function() {
    $(this)
        .find("h1,p")
        .addClass("fadeInDown animated");
  }); */
});
