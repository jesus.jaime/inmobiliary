$.renderMap = function() {
  var location = $("#DATA").data("url");
  $.ajax({
    type: "POST",
    url: location + "fetch_zones",
    data: {
      CRAFT_CSRF_TOKEN: $("input[name='CRAFT_CSRF_TOKEN']").val()
    },
    success: function(data) {
      var zoneList='';
      $.each(data,function(key,val){
        zoneList+= `
        
        <a href="${location}zonas/${val.slug}" class="text-center text-dark-grey">
          <li class="list-group-item rounded-0">${val.name}</li>
        </a>
        `
      });

      $('.zone-list-group').html(zoneList);

      var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 12,
        center: new google.maps.LatLng(
          20.612137,
          -100.410217
        ),
        mapTypeId: "roadmap"
      });

      var iconBase = $("#DATA").data("url");

      var icons = {
        kurgan: {
          icon: iconBase + "assets/img/miscellaneous/map_marker.png"
        }
      };

      var kg_markers = [];
      var kg_markers_caption = [];

      $.each(data, function(key, val) {
        //inline template

        var link = $("#DATA").data("url") +'zonas/'+ val.slug;
        var contentString = `
            <div class="py-3 px-1">
                <h3 class="text-dark text-center">${val.name}</h3>
                <img src="${val.image}" class="d-block mx-auto rounded shadow" height="150" />
                <div class="row text-center mt-3">
                  <div class="col-12">
                      <a target="_blank" href="${link}" class="btn btn-outline-dark-grey rounded-0">
                        <i class="fas fa-home"></i> Ver casas
                      </a>
                  </div>
                </div>
                
            </div>
        `;

        kg_markers_caption.push(
          new google.maps.InfoWindow({
            content: contentString
          })
        );

        kg_markers.push({
          position: new google.maps.LatLng(val.latitude, val.longitude),
          type: "kurgan"
        });
      });

      kg_markers.forEach(function(kg_marker, index) {
        var marker = new google.maps.Marker({
          position: kg_marker.position,
          icon: icons[kg_marker.type].icon,
          map: map,
          title: data[index].name
        });

        marker.addListener("click", function(map) {
          kg_markers_caption[index].open(map, marker);
        });
      });
    },
    error: function(error) {}
  });
};
$(function() {
  
});
