const { mix } = require('laravel-mix');
const Clean = require('clean-webpack-plugin');


mix.setPublicPath('web/assets');

if (mix.inProduction) {
    mix.disableNotifications();
}

if (!mix.inProduction) {
    mix.sourceMaps();
}

mix.webpackConfig({
    plugins: [
        new Clean(['web/assets/js', 'web/assets/css'], { verbose: false })
    ],
})

mix.options({
    processCssUrls: false
});

mix.sass('assets/scss/app.scss', 'web/assets/css/app.css');
mix.js('assets/js/app.js', 'web/assets/js/app.js');
mix.js('assets/js/index.js', 'web/assets/js/index.js');
mix.js('assets/js/property.js', 'web/assets/js/property.js');
mix.js('assets/js/properties.js', 'web/assets/js/properties.js');
mix.js('assets/js/zones.js', 'web/assets/js/zones.js');

mix.copy('assets/css/animate.css', 'web/assets/css/animate.css');

mix.version();

